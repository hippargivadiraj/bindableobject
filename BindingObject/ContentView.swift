//
//  ContentView.swift
//  BindingObject
//
//  Created by Leadconsultant on 11/28/19.
//  Copyright © 2019 Leadconsultant. All rights reserved.
//

import SwiftUI

struct AddView: View {
    @Binding var showPopUpView: Bool

    var body: some View {
        Button("Dismiss") {
            self.showPopUpView = false
        }
    }
}

struct ContentView: View {
 @State private var showPopUpView = false
    var body: some View {
        VStack{
        Text("Hello, World!")
            Button("ChangeIsPresented"){
                
                self.showPopUpView = true
            }
        }.sheet(isPresented: $showPopUpView) {
            AddView(showPopUpView: self.$showPopUpView)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
